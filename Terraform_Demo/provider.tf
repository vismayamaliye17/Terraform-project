terraform {
    backend "s3" {
        bucket = "vismaya-terraform"
        dynamodb_table = "state-lock"
        key = "global/mystatefile/terraform.tfstate"
        region = "us-east-1"
        encrypt = true
    }
}
