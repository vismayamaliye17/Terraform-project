provider "aws" {
    region = "us-east-1"
    access_key = "AKIAWXGGPB4GO3VC5KVX"
    secret_key = "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"

}

# resource "aws_instance" "first-instance" {
#     ami = "ami-05c13eab67c5d8861"
#     instance_type = "t2.micro"
#     tags = {
#         Name = "First Instance"
#     }
# }


# 1.Create vpc
resource "aws_vpc" "first-vpc" {
  cidr_block       = "10.0.0.0/16"

  tags = {
    Name = "first-vpc"
  }
}
# Create Internet Gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.first-vpc.id

  tags = {
    Name = "first-gw"
  }
}
# Create custom route table
resource "aws_route_table" "first-route-table" {
  vpc_id = aws_vpc.first-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  route {
    ipv6_cidr_block        = "::/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "route-table-1"
  }
}

# Create a subnet
resource "aws_subnet" "subnet-1" {
  vpc_id     = aws_vpc.first-vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "subnet-1"
  }
}
# associate subnet and route table
resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet-1.id
  route_table_id = aws_route_table.first-route-table.id
}
# Create Security Group to allow port 80, 22, 443
resource "aws_security_group" "allow-web" {
  name        = "allow_web"
  description = "Allow web inbound traffic"
  vpc_id      = aws_vpc.first-vpc.id

  ingress {
    description      = "HTTPS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"] 
  }
  ingress {
    description      = "HTTP from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"] 
  }
  ingress {
    description      = "SSH from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"] 
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow-web-server"
  }
}

# Create a network interface with an ip in the subnet created in step 4
resource "aws_network_interface" "first-network-interface" {
  subnet_id       = aws_subnet.subnet-1.id
  private_ips     = ["10.0.1.50"]
  security_groups = [aws_security_group.allow-web.id]

}
# Assign an elastic IP to the network interface
resource "aws_eip" "my-eip" {
  domain                    = "vpc"
  network_interface         = aws_network_interface.first-network-interface.id
  associate_with_private_ip = "10.0.1.50"
  depends_on = [aws_internet_gateway.gw]
  
}

# Create ubuntu server and install apache2
resource "aws_instance" "prod-instance" {
    ami = "ami-0fc5d935ebf8bc3bc"
    instance_type = "t2.micro"
    availability_zone = "us-east-1a"
    key_name = "ansible"
    

    network_interface {
        device_index = 0
        network_interface_id = aws_network_interface.first-network-interface.id
    }

    user_data = <<-EOF
                #!/bin/bash
                sudo apt update -y
                sudo apt install apache2 -y
                sudo systemctl start apache2
                sudo bash -c 'echo your very first web server > /var/www/html/index.html'
                EOF

}

